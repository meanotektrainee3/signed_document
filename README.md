1. Установить программу распознавания текста на картинке: tesseract https://medium.com/quantrium-tech/installing-tesseract-4-on-ubuntu-18-04-b6fcd0cbd78
2. Установить библиотеку для работы с tesseract: 
    pip install pytesseract
3. Установить pdf2image для конвертации pdf в картинки:
    pip install pdf2image