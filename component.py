#Модуль, в котором определяются компоненты свяязности на картинке

from neuthink.graph.basics import Graph
from neuthink.graph.basics import Node
import neuthink.metaimage as Image 
from neuthink import nImage as im  
from typing import Dict, Union, Tuple, List


def to_monochrome_img(imt: im.mvImage, threshold: int) -> im.mvImage:
    '''
        Возвращает картинку в черно-белых цветах без оттенков с заданным порогом

        Для преобразования изображения в оттенки серого, установить у imt свойство mode равный 'L': imt.mode = 'L'
        Далее преобразовать в монохромное изображение с заданным threshold
        и инвертировать: imt.Threshold(threshold=threshold,invert=True)
        Вернуть полученное изображение

        >>> imt = im.mvImage('files/example.png')
        >>> type(to_monochrome_img(imt, 95)) == im.mvImage
        True
    '''
    imt.mode='L'                #Convert image to grayscale                             
    
    return imt.Threshold(threshold=threshold,invert=True)    #Apply adaptive threshold to convert image to monochrome


def get_connected_components(imt: im.mvImage) -> List[Dict[str, Union[Tuple[Tuple], im.mvImage, Dict[str, Union[im.mvImage, int]], str, int]]]:
    '''
        Возвращает компоненты связности файла fileName

        Получить изображение по fileName в черно-белом виде без оттенков с порогом 95: to_monochrome_img
        Получить граф компонент с минимальным размером 30 пикселей: imt.GetConnectedObjects(minsize=30)
        Инициализировать basic graph
        Получить граф - дерево компонетов: Image.BasicSceneGraphParser(Node(graph,{'image':imt}),components),
        где graph - инициализированный basic graph
        Из полученного графа получить все узлы c типом image
        Для всех полученных узлов установить ['target_class']= 'letter'
        Если среди полученных узлов есть те, у которых ширина и высота полученные 
        по ключу ['image'].size больше 100 и 50 пикселей соответственно, тогда:
            Объединить компоненты, близкие друг к другу: Image.JoinBoundingBoxes(all_images, min_distance=10, comp_class='letter'),
            где all_images - полученные ранее узлы image с установленным target_class
            Получить узлы из graph c типом image и вернуть их
        Иначе вернуть пустой список

        >>> len(get_connected_components(im.mvImage('files/OMWealth(2).jpg')))
        294
        RIGHT MERGE RUNNING
        61
    '''
    imt = to_monochrome_img(imt, 95)

    components = imt.GetConnectedObjects(minsize=30)   #This gets all connected components with size>30 pixels in any dimension
    scene = Graph()             

    Image.BasicSceneGraphParser(Node(scene,{'image':imt}),components) #Creates a scene tree with all images (see explanation_of_scene_tree.png for more info)"
    all_images = scene.Match({'type':'image'})
    connect_components = False

    for image in all_images:
        width, height = image['image'].size

        connect_components = True if width > 100 and height > 50 else connect_components
        image['target_class']= 'letter'

    if connect_components: 
        Image.JoinBoundingBoxes(all_images, min_distance=15, comp_class='letter') #Joins neaby objects together

    return scene.Match({'type':'image'})


if __name__ == "__main__":
    import doctest

    doctest.testmod()