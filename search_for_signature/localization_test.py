import neuthink.metaimage as Image 
from neuthink.graph.basics import Graph,Node
from is_handwriting_aug import  is_handwriting_aug as is_handwriting2
#from is_handwriting2 import  is_handwriting2 as is_handwriting2
#from is_handwriting import  is_handwriting as is_handwriting2
from PIL import ImageDraw

data = Image.Load('files/test').Image.Mode().model 
for x in data:
   x['original_image'] = x['image']
   x['image'] =    x['image'].Threshold(threshold=90)
   print('************************')
   print(type(x['original_image']))
print(data)
image = data[0]['image']
original = data[0]['original_image']
print(type(image))
components = image.GetConnectedObjects(minsize=256)
g = Graph()             
n = Node(g,{'image':image})
pnode = Image.BasicSceneGraphParser(n,components)  
images = pnode.parent_graph.Match({'type':'image'})
print(len(images))
images = is_handwriting2(images)
#Image.JoinBoundingBoxes(images,min_distance=10,comp_class='hand-hi')
nodes = images.Match({'target_class':'hand-hi'}).NotMatch({'_state_delete': 'yes'})
print('************************')
print(len(nodes))
q = original.content.convert('RGB')
draw = ImageDraw.Draw(q)
for x in nodes:

  coord = x["location"]
  print(coord)
  draw.rectangle(coord,outline='red')

q.show()
q.save("demo1.png")
input() 

print(nodes[0:4])
