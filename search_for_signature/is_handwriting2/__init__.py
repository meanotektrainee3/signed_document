from neuthink import metagraph as m
from neuthink.graph.basics import Graph
import torch
import os
graph = Graph()
path = os.path.dirname(os.path.abspath(__file__))
print(path)
model = m.dNodeList(graph)
model.classes = [x.strip() for x in open(path+'/'+'is_handwriting2.cls','r').readlines()]
model.Image.Resize(scale=(64, 32),source="image",target="image")
model.Image.Mode(mode="L",source="image",target="image")
model.Image.Vectorize(source="image",target="image_tensor")
model.Flatten(target="image_tensor",source="image_tensor")
model.Linear(size=256,source="image_tensor",target="inter",input_size=2048)
model.Sigmoid(source="inter",target="inter")
model.Classify(target="dMap1",class_target="target_class",cmode="Normal",source="inter",input_size=256)
model.Rename(source="dMap1",target="target_class")

model.load_state_dict(torch.load(path +'/'+'is_handwriting2.mod'))
model.mode='predict'
is_handwriting2 = lambda x: model.predict(x)
