#Модуль находит все поля с подписями на картинке и определяет подписан ли он

from neuthink import nImage as im  
import neuthink.metaimage as Image 
from PIL import ImageDraw
from neuthink.graph.basics import Graph,Node
#from search_for_signature.is_handwriting_aug import  is_handwriting_aug as is_handwriting2
#from search_for_signature.is_handwriting2 import  is_handwriting2 as is_handwriting2
from search_for_signature.is_handwriting import  is_handwriting as is_handwriting2
from math import sqrt, pow
from typing import Union, List, Dict, Tuple
import pytesseract


def is_signature(img: im.mvImage) -> bool:
    '''
        Определяет текст на рисунке, если на нем (не)написано 'signature', возвращает (False)True

        С помощью программы tesseract определяет, что написано на рисунке img
        Переводит в нижний регистр и удаляет пробелы и переносы строк в начале и конце
        Возвращает True, если полученная строка равна 'signature', иначе - False

        >>> imt = im.mvImage('files/signature_word.png')
        >>> is_signature(imt)
        True
        >>> imt = im.mvImage('files/one_word.png')
        >>> is_signature(imt)
        False
    '''
    pass

def get_distance(x0: int, y0: int, x1: int, y1: int) -> float:
    '''
        Вычисляет расстояние от точки x0, y0 до x1, y1

        Вычислить расстояние использую формулу √((x0- x1)^2 + (y0 - y1)^2)
        
        >>> get_distance(1, 1, 5, 6)
        6.4031242374328485
    '''
    return sqrt(pow(x0 - x1, 2) + pow(y0 - y1, 2))

def get_area(x0: int, y0: int, x1: int, y1: int) -> int:
    '''
        Вычисляет площадь прямоугольника по координатам верхней левой точки и нижней правой: 
        (x0, y0) и (x1, y1) соответственно

        Вычислить площадь использую формулу |x1- x0| * |y1 - y0|
        
        >>> get_area(1, 2, 5, 6)
        16
    '''
    pass


def get_near_components(cur_component: Dict[str, Union[Tuple[Tuple], im.mvImage, Dict[str, Union[im.mvImage, int]], str, int]], components: List[Dict[str, Union[Tuple[Tuple], im.mvImage, Dict[str, Union[im.mvImage, int]], str, int]]]) -> List[Dict[str, Union[Tuple[Tuple], im.mvImage, Dict[str, Union[im.mvImage, int]], str, int]]]:
    '''
        Возвращает список компонентов из components, которые находятся на расстоянии не более 20 пикселей от компонента cur_component

        Получить кортеж с координатами cur_component взяв у него значение по ключу 'location'
        Присвоить переменным x и y значения элемента первого элемента полученного кортежа
        Пройтись по всем списку компонент, для каждого:
            Если значение по ключу id у cur_component не совпадает со значением id текущего элемента:
                Получить кортеж с координатами взяв у текущего элемента значение по ключу 'location'
                Присвоить переменным x_i и y_i значения элемента первого элемента полученного кортежа
                Найти расстояние между точками (x, y) и (x_i, y_i)  - get_distance
                Если расстояние меньше 20, то добавить текущий элемент в результирующий список
        Вернуть результирующий список

        >>> get_near_components({'id': 0, 'location' : ((0, 0), (1, 1))}, [{'id': 0, 'location' : ((0, 0), (1, 1))}, {'id': 1, 'location' : ((10, 10), (11, 11))}, {'id': 2, 'location' : ((100, 200), (110, 210))}])
        [{'id': 1, 'location': ((10, 10), (11, 11))}]
    '''
    pass


def get_biggest_component(components: List[Dict[str, Union[Tuple[Tuple], im.mvImage, Dict[str, Union[im.mvImage, int]], str, int]]]) -> Dict[str, Union[Tuple[Tuple], im.mvImage, Dict[str, Union[im.mvImage, int]], str, int]]:
    '''
        Возвращает компонент с самой большой площадью

        Для каждого элемента из components:
            Взять в качестве значений координат ((x0, y0), (x1, y1)) значения по ключу 'location'
            Рассчитать площать компонента get_area(x0, y0, x1, y1)
        Вернуть компонент с наибольшей площадью, удовлетворяющий условиям:
            Высота прямоугольника меньше ширины, при том, что
            (x0, y0) - координаты верхней левой точки, (x1, y1) - нижней правой точки прямоугольника
            Минимальная ширина прямоугольника 30 пикселей, максимальная 200
            Минимальная высота прямоугольника 30 пикселей. максимальная 100

        >>> get_biggest_component([{'id': 0, 'location' : ((0, 0), (150, 80))}, {'id': 1, 'location' : ((10, 10), (11, 11))}, {'id': 2, 'location' : ((100, 200), (110, 210))}])
        {'id': 0, 'location': ((0, 0), (150, 80))}
    '''
    pass


def get_signature_field(components: List[Dict[str, Union[Tuple[Tuple], im.mvImage, Dict[str, Union[im.mvImage, int]], str, int]]]) -> List[Dict[str, Union[Tuple[Tuple], im.mvImage, Dict[str, Union[im.mvImage, int]], str, int]]]:
    '''
        Из списка компонент связности находит те, что представляют из себя поле для подписи, возвращает в виде списка

        Пройтись по всему списку, для каждого элемента:
            Взять картинку по ключевому слову image
            Определить, написано ли на ней слово signature (is_signature)
            Если да, то:
                Найти ближайший компонент, который представляет собой поле для подписи get_near_components
                передав в качестве первого аргумента текущий элемент и в качестве второго - components
                Из полученного списка компонент найти самый большой по площади - get_biggest_component
                Добавить полученный элемент в результирующий список
        Вернуть полученный список компонент
    '''
    pass


if __name__ == "__main__":
    import doctest
    
    doctest.testmod()