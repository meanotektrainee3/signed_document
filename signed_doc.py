#Модуль для определения все ли поля для подпиcи в pdf документе заполнены
from PIL.PpmImagePlugin import PpmImageFile
from pdf2image import convert_from_path
from PIL import Image as pilImage
from neuthink.nImage  import mvImage
import neuthink.metaimage as Image 
from typing import List, Dict, Union, Tuple
import signature as sign 
import component as comp
import os
    

def to_img(fileName: str) -> List[mvImage]:
    '''
        Возвращает список страниц в виде картинок в файле fileName

        Используя pdf2image конвертировать pdf файл fileName в список картинок
        Пройтись по полученному списку, фомируя новый список из элементов типа mvImage, конвертируя
        исходный элемент в mvImage: mvImage("",in_memory=True, source = page), где
        page - очередной элемент из списка картинок
        Вернуть новый список
        
        >>> len(to_img('files/2-in-one.pdf')) > 0
        True
    '''
    pass


def cut_pic(img: pilImage, first_half: bool) -> pilImage:
    '''
        Образает по высоте картинку пополам и возвращает первую половину, если first_half, иначе - вторую часть

        Получает оригинальные высоту и ширину картинки в пикселях(width, height = img.size)
        Вычисляет в пикселях половину высоты(half_height)
        Возвращает обрезанную картинку по высоте(img.crop(x0, y0, x1, y1)): 
        первую половину(x0 = 0, y0 = 0, x1 = wigth, y1 = half_height), если first_half, 
        иначе - вторую часть, нижнюю(x0 = 0, y0 = half_height, x1 = wigth, y1 = height)

        >>> orig_img = pilImage.open('files/OMWealth_full_list(2).jpg')
        >>> img2 = cut_pic(orig_img, False)
        >>> float(img2.size[1]) == orig_img.size[1] / 2
        True
    '''
    pass


def is_signed(components: List[Dict[str, Union[bool, Tuple[Tuple], mvImage, Dict[str, Union[mvImage, int]], str, int]]]) -> bool:
    '''
        Возвращает True, если все поля для подписи заполнены, иначе - False

        У каждого элемента из components проверяет значение по ключу 'signed'
        Если для всех элементов это значаение равняется True, возвращает True, 
        иначе - False
        Если список components пустой, вернуть True 

        >>> is_signed([{'id': 0, 'signed': True}, {'id': 1, 'signed': True}])
        True
        >>> is_signed([{'id': 0, 'signed': False}, {'id': 1, 'signed': True}])
        False
        >>> is_signed([])
        True
    '''
    pass


def clean_temp_folder() -> None:
    '''
        Очищает каталог temp в текущем каталоге

        Проходит по всем файлам в папке temp и удаляет их        
        
        >>> clean_temp_folder() 
        >>> len(os.listdir('temp/'))
        0
    '''
    pass


def get_file_name(file_name: str) -> str:
    '''
        Возвращает название файла с расширением, но без пути к этому файлу

        Разделить file_name на элементы по символу '/', взять последний элемент

        >>> get_file_name('/manager/git.py')
        'git.py'
    '''
    pass


def save_file(image: mvImage) -> str:
    '''
        Сохраняет файл image во временную папку temp, возвращает его полный путь с названием

        Получить количество файлов в папке temp
        Сохранить файл image в папку temp c названием: file(i).png,
        где i = количество файлов в папке temp
        Вернуть 'temp/' + file(i).png

        >>> orig_img = pilImage.open('files/OMWealth_full_list(2).jpg')
        >>> i = len(os.listdir('temp/'))
        >>> img = mvImage("",in_memory=True,source=orig_img)
        >>> res = save_file(img)
        >>> i == len(os.listdir('temp/')) - 1
        True
    '''
    pass


if __name__ == "__main__":
    import doctest

    doctest.testmod()

